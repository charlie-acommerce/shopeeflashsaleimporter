import json

my_sheet_data = [
    [345, 334, 344, 333],
    ["shopeeth", "shopeeth", "shopeeth", "shopeeth"],
    ["ShopeeFlashSale", "ShopeeFlashSale", "ShopeeFlashSale", "ShopeeFlashSale"],
    ["123", "456", "789", "900"],
    ["133", "556", "889", "990"],
    [20324, 2334234, 10023423, 877234],
    [12, 23, 34, 65],
    [11, 22, 33, 55],
]

schema = [
    "partnerId",
    "channelId",
    "promotionName",
    "startDate",
    "endDate",
    "productSku",
    "basePrice",
    "finalPrice"
]
my_dict_list = []
for row in zip(*my_sheet_data):
    d = dict(zip(schema, row))
    my_dict_list.append(d)
print(my_dict_list)
print(json.dumps(my_dict_list, sort_keys=True, indent=4))
