SELECT ccp.sku 
FROM datawarehouse.dim_channel_product cp
LEFT JOIN datawarehouse.dim_channel_child_product ccp ON (ccp.channel_product_key = cp.channel_product_key)
where 
  cp.seller_id = %s
  AND cp.channel_sku = %s 
  AND cp.channel_updated_time >= %s
  order by ccp.channel_updated_time
  limit 1
