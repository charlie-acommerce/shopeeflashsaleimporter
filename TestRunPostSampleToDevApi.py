import pendulum
from openpyxl import load_workbook
import requests
import pprint
from urllib.parse import quote_plus, urlencode
import json
import psycopg2

'''
workbook = load_workbook(
    filename="C:/Users/charlie.b/Downloads/Shopee Flash Sale Data Sample.xlsx"
)
worksheet = workbook["CFS"]


# creating the list of start datetime and end datetime:
def get_start_end_lists(start_date_list, end_date_list):
    print("Fetching Start and End DateTimes:")

    year_list = []
    month_list = []
    day_list = []
    start_time_list = []
    end_time_list = []

    for cell in worksheet["A"]:
        if cell.value != None and cell.value != "year":
            year_list.append(cell.value)

    for cell in worksheet["B"]:
        if cell.value != None and cell.value != "month":
            month_list.append("0" + str(cell.value))

    for cell in worksheet["D"]:
        if cell.value != None and cell.value != "date_id":
            day_list.append(cell.value)

    for cell in worksheet["E"]:
        if cell.value != None and cell.value != "start_time":
            start_time_list.append(cell.value)

    for cell in worksheet["F"]:
        if cell.value != None and cell.value != "sold_out_time":
            end_time_list.append(cell.value)

    # returning only digits for 'day'
    for i, val in enumerate(day_list):
        day = ""
        for char in val:
            if char.isdigit():
                day = day + char
                day_list[i] = day

    counter = 0
    while counter <= len(year_list) - 1:
        start_dt_string = (
            str(year_list[counter])
            + "-"
            + str(month_list[counter])
            + "-"
            + str(day_list[counter])
            + "T"
            + str(start_time_list[counter])
        )

        end_dt_string = (
            str(year_list[counter])
            + "-"
            + str(month_list[counter])
            + "-"
            + str(day_list[counter])
            + "T"
            + str(end_time_list[counter])
        )
        start_dt_string = str(pendulum.parse(start_dt_string, tz="UTC"))
        end_dt_string = str(pendulum.parse(end_dt_string, tz="UTC"))

        start_date_list.append(start_dt_string)
        end_date_list.append(end_dt_string)

        counter += 1

    print("Fetched DateTimes Successfully")


# requesting channel and partner id from sellermaster, and returning the lists:
def get_channelid_partnerid_lists(channelid_list, partnerid_list, error_row_list):
    print("Requesting Partner And Channel ID's from Sellermaster:")

    shopid = ""

    url = "http://sellermaster.acommerce.service/internal/sellers"

    for cell in worksheet["H"]:
        if cell.value != None and cell.value != "shopid":
            shopid = cell.value

            query_params = urlencode(
                {"channel": "shopeeth", "channel_seller_id": shopid}
            )
            full_url = url + "?" + query_params
            response = requests.get(full_url)

            json_data = response.json()

            # if json response isn't empty list
            if json_data != []:
                channelid_list.append(json_data[0]["channelId"])
                partnerid_list.append(json_data[0]["partnerIds"][0])
            else:
                row_number = len(channelid_list) + 2
                error_row_list.append(row_number)

                # have to append a value to this list so error row count stays correct, this value is deleted after this function
                partnerid_list.append("")
                channelid_list.append("")

                print(
                    "{",
                    "  error message: seller does not exist",
                    "  error row number: " + str(row_number),
                    "}",
                    "",
                    sep="\n",
                )

    print("All valid ID's have been fetched successfully")


def extract_sprdsheet_data(channel_sku_list, displayed_rsp_list, promotion_name_list):
    print("Extracting remaining required data from spreadsheet:")
    # list of itemid's:
    for cell in worksheet["J"]:
        if cell.value != None and cell.value != "itemid":
            channel_sku_list.append(str(cell.value))

    # list of base price:

    for cell in worksheet["S"]:
        # hardcoded to count to no of rows for this sheet as some cells have null values (quickest fix)
        if cell.value != "seller_offer_price" and cell.row <= len(channel_sku_list) + 1:
            if cell.value != "":
                displayed_rsp_list.append(str(cell.value))
            else:
                # call function to calculate what displayed_rsp should be for missing values
                discount_percentage = worksheet["Q"][cell.row - 1].value
                final_price = worksheet["P"][cell.row - 1].value
                get_missing_rsp(displayed_rsp_list, discount_percentage, final_price)

    for cell in worksheet["P"]:
        if cell.value != None and cell.value != "promotion_price":
            final_price_list.append(str(cell.value))
            promotion_name_list.append("ShopeeFlashSale")

    print("Extracted successfully")


def get_missing_rsp(displayed_rsp_list, discount_percentage, final_price):

    displayed_rsp = round(final_price / (1 - discount_percentage))
    displayed_rsp_list.append(str(displayed_rsp))


def get_sku_from_supplychain(
    channel_sku_list, start_date_list, product_sku_list, error_row_list
):

    print("Connecting to Data Warehouse, Requesting SKU")

    try:
        connection = psycopg2.connect(
            user="app_shopee_importer",
            password="NwUCUskANUHbx74p",
            host="bidb.acommerce.platform",
            port="5439",
            database="supplychain",
        )

        cursor = connection.cursor()
        # Print Connection properties
        print(connection.get_dsn_parameters(), "\n")

        qry_sellerid = open("qry_get_sellerid.sql", "r").read()
        qry_sku = open("qry_get_sku.sql", "r").read()

        for idx, channel_sku in enumerate(channel_sku_list):

            cursor.execute(qry_sellerid, (channel_sku,))
            sellerid = cursor.fetchone()

            row_number = len(product_sku_list) + 2

            if sellerid is None:
                product_sku_list.append(None)

                error_row_list.append(row_number)

                print("Current Row:", row_number)
                print("SellerId :", sellerid)               
                print("")

                print(
                    "{",
                    "  error message: sellerid does not exist",
                    "  error row number: " + str(row_number),
                    "}",
                    "",
                    sep="\n",
                )

            else:

                args = sellerid[0], channel_sku, start_date_list[idx]
                cursor.execute(qry_sku, args)
                sku = cursor.fetchone()
         
         
                if "None" in str(sku):
                    error_row_list.append(row_number)
                    product_sku_list.append(None)

                    print("Current Row:", row_number)
                    print("SellerId :", sellerid)
                    print("Sku :", sku)
                    print("")

                    print(
                        "{",
                        "  error message: sku does not exist",
                        "  error row number: " + str(row_number),
                        "}",
                        "",
                        sep="\n",
                    )
                else:
                    product_sku_list.append(sku[0])

    except (Exception, psycopg2.Error) as error:
        print("Error while connecting to PostgreSQL", error)
        if connection:
            cursor.close()
            connection.close()
            print("PostgreSQL connection is closed")
            exit()

    finally:
        if connection:
            cursor.close()
            connection.close()
            print("PostgreSQL connection is closed")


def remove_error_data(
    error_row_list,
    partnerid_list,
    channelid_list,
    promotion_name_list,
    start_date_list,
    end_date_list,
    product_sku_list,
    displayed_rsp_list,
    final_price_list,
):

    print("Removing error data from extracted data sets")

    # removing duplicate values in error row list and sorting
    error_row_list = list(dict.fromkeys(error_row_list))
    error_row_list.sort()

    print("Error Count:", len(error_row_list))
    index_adjust = 0
    for elem in error_row_list:
        index = int(elem - (2 + index_adjust))
        del partnerid_list[index]
        del channelid_list[index]
        del promotion_name_list[index]
        del start_date_list[index]
        del end_date_list[index]
        del product_sku_list[index]
        del displayed_rsp_list[index]
        del final_price_list[index]

        index_adjust += 1

    print("Error instances have been removed before Json Dump")


def create_grouped_promotions(promos_dict_list):    

    for promo1 in promos_dict_list:
        for idx, promo2 in enumerate(promos_dict_list):
            if (
                promo1 != promo2
                and promo1["partnerId"] == promo2["partnerId"]
                and promo1["startDate"] == promo2["startDate"]
                and promo1["endDate"] == promo2["endDate"]
            ):
                promo1["productPrices"].append(promo2["productPrices"][0])
                del promos_dict_list[idx]

        promo_Name = (
            "ShopeeFlashSale PartnerId: "
            + promo1["partnerId"]
            + " ("
            + pendulum.parse(promo1["startDate"]).to_datetime_string()
            + " - "
            + pendulum.parse(promo1["endDate"]).to_datetime_string()
            + ")"
        )

        promo1["promotionName"] = promo_Name

    return promos_dict_list


def json_dump(promotion_data_list, product_prices_list, product_prices_dict):
    print("Dumping to Json file")

    schema_1 = [
        "productPrices",
        "partnerId",
        "channelId",
        "promotionName",
        "startDate",
        "endDate",
    ]

    schema_2 = ["productSku", "displayedRsp", "finalPrice"]

    promos_dict_list = []

    for row in zip(*product_prices_list):
        d = dict(zip(schema_2, row))
        product_prices_dict.append(
            [d,]
        )

    for row in zip(*promotion_data_list):
        d = dict(zip(schema_1, row))
        promos_dict_list.append(d)

    create_grouped_promotions(promos_dict_list)

    json_file = open("Output_json_Test.json", "w")

    json_file.write(json.dumps(promos_dict_list, sort_keys=True, indent=4))

    json_file.close()

    print("Successfully dumped Json to file")


channel_sku_list = []

partnerid_list = []
channelid_list = []
start_date_list = []
end_date_list = []
product_sku_list = []
displayed_rsp_list = []
final_price_list = []
promotion_name_list = []


error_row_list = []

product_prices_list = [
    product_sku_list,
    displayed_rsp_list,
    final_price_list,
]

product_prices_dict = []

promotion_data_list = [
    product_prices_dict,
    partnerid_list,
    channelid_list,
    promotion_name_list,
    start_date_list,
    end_date_list,
]

get_start_end_lists(start_date_list, end_date_list)

get_channelid_partnerid_lists(channelid_list, partnerid_list, error_row_list)

extract_sprdsheet_data(channel_sku_list, displayed_rsp_list, promotion_name_list)

get_sku_from_supplychain(
    channel_sku_list, start_date_list, product_sku_list, error_row_list
)

remove_error_data(
    error_row_list,
    partnerid_list,
    channelid_list,
    promotion_name_list,
    start_date_list,
    end_date_list,
    product_sku_list,
    displayed_rsp_list,
    final_price_list,
)

json_dump(promotion_data_list, product_prices_list, product_prices_dict)

'''
def push_json_to_api():
  
    url = "http://cm-promotion-api-dev.acommercedev.service/promotion-api/private/price-promotions/"

    with open("Output_json_Test.json", "r") as data_file:
        json_file = json.load(data_file)
        prom_counter = 1

        for promotion in json_file:
            headers = {
                "X-Roles": "bo_" + str(promotion["partnerId"]),
                "X-User-Name": "flash-sale-importer",
            }

            r_post = requests.post(url, headers=headers, json=promotion)

            print("Pushing promotion: ", prom_counter, "/", len(json_file), "\n")
            if str(r_post) != "<Response [200]>":
                print(" ", r_post.json())
                for item1 in promotion["productPrices"]:
                    for idx, item2 in enumerate(promotion["productPrices"]):
                        if item2["productSku"] == item1["productSku"]:
                            del promotion["productPrices"][idx]
                            
                print("  Duplicate promotions removed\n  Trying again:")
                r_post = requests.post(url, headers=headers, json=promotion)
                print(" ", r_post, "\n")
            prom_counter += 1

push_json_to_api()



