import requests
import json


url = "http://cm-promotion-api-dev.acommercedev.service/promotion-api/private/price-promotions/"


with open("sampleJson.json", "r") as data_file:
    json_file = json.load(data_file)
    prom_counter = 1

    for promotion in json_file:
        headers = {
            "X-Roles": "bo_" + str(promotion["partnerId"]),
            "X-User-Name": "flash-sale-importer",
        }

        r_post = requests.post(url, headers=headers, json=promotion)

        print("Pushing promotion: ", prom_counter, "/", len(json_file), "\n")
        if str(r_post) != "<Response [200]>":
            print(" ", r_post.json())

        print(" ", r_post, "\n")
        prom_counter += 1